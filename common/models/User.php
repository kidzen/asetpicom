<?php

namespace common\models;

use Yii;
use \common\models\base\User as BaseUser;

/**
 * This is the model class for table "user".
 */
class User extends BaseUser
{
    /**
     * @inheritdoc
     */
    public function testpublic($quantity)
    {

        return $quantity;
    }

    public static function teststatic($quantity)
    {
        return $quantity;
    }

    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['username', 'staff_no', 'auth_key', 'password_hash', 'email'], 'required'],
            [['role_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['username', 'staff_no', 'profile_pic', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['staff_no'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique']
        ]);
    }

}
