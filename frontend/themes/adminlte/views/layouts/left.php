<?php
use yii\helpers\Url;
// $data = Yii::$app->db->getSchema()->getTableNames();
// var_dump($data);
// die();

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div style="text-align: center">
                <img src="<?= Yii::getAlias('@web/').Url::to('images/PICOMS.jpg') ?>" style="width: 100%" alt="Mohor_rasmi_Majlis_Perbandaran_Seberang_Perai"/>
                <!--<img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" style="width: 200px" alt="TUDM"/>-->
            </div>
        </div>


        <!-- search form -->
<!--         <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
 -->        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Log Masuk', 'icon' => 'share', 'url' => ['/site/login'],
                        // 'visible' => Yii::$app->user->isGuest,
                    ],
                    [
                        'label' => 'Permohonan', 'icon' => 'stack-overflow',
                        // 'visible' => !Yii::$app->user->isGuest,
                        'items' => [
                            ['label' => 'Daftar Aset', 'icon' => 'download', 'url' => ['/asset-item/create']],
                            ['label' => 'Daftar Jenis Aset', 'icon' => 'download', 'url' => ['/asset/create']],
                            ['label' => 'Daftar Kelas Aset', 'icon' => 'download', 'url' => ['/category/create']],
                            ['label' => 'Daftar Lokasi', 'icon' => 'download', 'url' => ['/location/create']],
                        ]
                    ],
                    [
                        'label' => 'Rekod', 'icon' => 'stack-overflow',
                        // 'visible' => !Yii::$app->user->isGuest,
                        'items' => [
                            ['label' => 'Kelas Aset', 'icon' => 'calendar-check-o', 'url' => ['/category/index']],
                            ['label' => 'Jenis Aset', 'icon' => 'balance-scale', 'url' => ['/asset/index']],
                            ['label' => 'Aset', 'icon' => 'balance-scale', 'url' => ['/asset-item/index']],
                        ]
                    ],
                    [
                        'label' => 'Pentadbiran', 'icon' => 'cogs',
                        // 'visible'=>false,
                        'visible' => !Yii::$app->user->isGuest && !Yii::$app->user->isAdmin,
                        'items' => [
                            ['label' => 'Pengguna', 'icon' => 'users', 'url' => ['/user/view','id'=>Yii::$app->user->id],],
                            ['label' => 'Profil', 'icon' => 'user', 'url' => ['/profile/view','id'=>Yii::$app->user->id]],
                            // ['label' => 'Senarai Status Draf', 'icon' => 'balance-scale', 'url' => ['/draft-status/index']],
                        ]
                    ],
                    [
                        'label' => 'Pentadbiran', 'icon' => 'cogs',
                        // 'visible'=>false,
                        // 'visible' => Yii::$app->user->isAdmin,
                        'items' => [
                            ['label' => 'Pengguna', 'icon' => 'users', 'url' => ['/user/index'],],
                            // ['label' => 'Profil', 'icon' => 'user', 'url' => ['/profile/index']],
                            ['label' => 'Akses', 'icon' => 'lock', 'url' => ['/role/index']],
                        ]
                    ],
                    [
                        'visible'=>false,
                        'label' => 'Dev tools',
                        'icon' => 'share',
                        // 'url' => '#',
                        'items' => [
                            [
                                'label' => 'Database',
                                'icon' => 'database',
                                'items' => [
                                        ['label' => 'User', 'icon' => 'user', 'url' => ['/user/index']],
                                        ['label' => 'Profile', 'icon' => 'user', 'url' => ['/profile/index']],
                                        ['label' => 'Akses', 'icon' => 'user', 'url' => ['/role/index']],
                                        ['label' => 'Kontrak', 'icon' => 'user', 'url' => ['/contract/index']],
                                        ['label' => 'Draft Template', 'icon' => 'user', 'url' => ['/draft-template/index']],
                                        ['label' => 'Contract Template Assignment', 'icon' => 'user', 'url' => ['/contract-template-assignment/index']],
                                        ['label' => 'Senarai Pemboleh Ubah', 'icon' => 'user', 'url' => ['/param-list/index']],
                                        ['label' => 'Draft Param', 'icon' => 'user', 'url' => ['/draft-param/index']],
                                        ['label' => 'Draft Document', 'icon' => 'user', 'url' => ['/draft-document/index']],
                                        ['label' => 'Date Setting', 'icon' => 'user', 'url' => ['/date-setting/index']],
                                        ['label' => 'Document Checklist', 'icon' => 'user', 'url' => ['/document-checklist/index']],
                                        ['label' => 'Document Checklist Lookup', 'icon' => 'user', 'url' => ['/document-checklist-lookup/index']],
                                        ['label' => 'Status Draf', 'icon' => 'user', 'url' => ['/draft-status/index']],
                                        ['label' => 'Note', 'icon' => 'user', 'url' => ['/note/index']],
                                        ['label' => 'Approve Level', 'icon' => 'user', 'url' => ['/approve-level/index']],
                                        ['label' => 'Approver', 'icon' => 'user', 'url' => ['/approver/index']],
                                        ['label' => 'Migration', 'icon' => 'user', 'url' => ['/migration/index']],
                                ],
                            ],
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                        ],
                    ],
                    // ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    // ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    // ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'visible'=>false,
                        'label' => 'Same tools',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
