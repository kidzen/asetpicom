<?php
use backend\assets\AppAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

dmstr\web\AdminLteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="login-page img-layout">

<?php $this->beginBody() ?>

    <?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php
// var_dump($_SERVER);die();
// var_dump(Yii::$app->request->hostInfo.Yii::$app->request->baseUrl);die();
// var_dump(Yii::$app->request->hostInfo);die();
// var_dump(Yii::$app->request->url);die();
// var_dump(Yii::$app->request);die();
?>
<?php $this->registerCss('
.img-layout{
	// background-color:green;
	background-image:url("'. Yii::getAlias('@web') .'/images/estor4.jpg");
	// background-image:url("http://localhost/dakwaan/frontend/web/images/MPSP5.jpg");
	background-size:100%;
}

') ?>
<?php $this->endPage() ?>
