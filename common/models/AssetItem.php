<?php

namespace common\models;

use Yii;
use \common\models\base\AssetItem as BaseAssetItem;

/**
 * This is the model class for table "asset_item".
 */
class AssetItem extends BaseAssetItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['asset_id'], 'required'],
            [['asset_id', 'location_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['puchased_date', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['rate', 'cost_movement', 'depreciation_movement'], 'number'],
            [['asset_no'], 'string', 'max' => 255]
        ]);
    }
	
}
