<?php

namespace common\models;

use Yii;
use \common\models\base\Asset as BaseAsset;

/**
 * This is the model class for table "asset".
 */
class Asset extends BaseAsset
{
    /**
     * @inheritdoc
     */
    public static function insertTest($id,$name)
    {
        $object =  new static();
        $object->id = $id;
        $object->name = $name;
        $object->save();

    }
    public static function deleteOne($id)
    {
        static::findOne($id)->delete();

    }
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['category_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'required'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255]
        ]);
    }

}
