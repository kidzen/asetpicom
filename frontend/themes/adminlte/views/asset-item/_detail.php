<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\AssetItem */

?>
<div class="asset-item-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'asset.name',
            'label' => 'Asset',
        ],
        [
            'attribute' => 'location.name',
            'label' => 'Location',
        ],
        'asset_no',
        'puchased_date',
        'rate',
        'cost_movement',
        'depreciation_movement',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>