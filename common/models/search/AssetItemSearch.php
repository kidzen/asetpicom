<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AssetItem;

/**
 * common\models\search\AssetItemSearch represents the model behind the search form about `common\models\AssetItem`.
 */
 class AssetItemSearch extends AssetItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'asset_id', 'location_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['asset_no', 'puchased_date', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['rate', 'cost_movement', 'depreciation_movement'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AssetItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'asset_id' => $this->asset_id,
            'location_id' => $this->location_id,
            'puchased_date' => $this->puchased_date,
            'rate' => $this->rate,
            'cost_movement' => $this->cost_movement,
            'depreciation_movement' => $this->depreciation_movement,
            'status' => $this->status,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'asset_no', $this->asset_no]);

        return $dataProvider;
    }
}
